#!/bin/bash
apt-get update
apt-get install sudo wget
#curl -sSL https://get.docker.com/ | sh
echo deb http://get.docker.com/ubuntu docker main > /etc/apt/sources.list.d/docker.list
apt-key adv --keyserver pgp.mit.edu --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
apt-get update
apt-get install -y lxc-docker-1.6.2
wget -q -O - https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | sudo apt-key add -
echo "deb http://packages.cloudfoundry.org/debian stable main" | sudo tee /etc/apt/sources.list.d/cloudfoundry-cli.list
apt-get update
apt-get install cf-cli
cf install-plugin https://static-ice.ng.bluemix.net/ibm-containers-linux_x64

cf login -a https://api.ng.bluemix.net
cf target -o $org
cf target -s dev
cf ic namespace set $(openssl rand -base64 8 | md5sum | head -c8)
cf ic init

mkdir ss
cd ss

cat << _EOF2_ >config.json
{
    "server":"0.0.0.0",
    "server_port":443,
    "local_address": "127.0.0.1",
    "local_port":1080,
    "password":"Kh#p*378V",
    "timeout":300,
    "method":"aes-256-cfb",
    "fast_open": false
}
_EOF2_

cat << _EOF_ >Dockerfile
FROM alpine:latest
RUN set -ex && apk add --no-cache libsodium py2-pip && pip --no-cache-dir install https://github.com/shadowsocks/shadowsocks/archive/master.zip
ADD config.json /config.json
EXPOSE 443
ENTRYPOINT ["ssserver", "-c", "/config.json"]
_EOF_

cf ic build -t ss:v1 .
cf ic ip bind $(cf ic ip request | cut -d \" -f 2 | tail -1) $(cf ic run -m 1024 --name=ss -p 443 registry.ng.bluemix.net/`cf ic namespace get`/ss:v1)